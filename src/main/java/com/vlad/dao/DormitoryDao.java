package com.vlad.dao;

import com.vlad.model.Dormitory;
import com.vlad.model.Subject;

import java.util.List;

public interface DormitoryDao {
    public Dormitory getDormitory(Long id);


    public Long saveDormitory(Dormitory sb);


    public List<Dormitory> listAllDormitory();


    public void updateDormitory(Dormitory sb);


    public void deleteDormitory(Dormitory sb);
}
