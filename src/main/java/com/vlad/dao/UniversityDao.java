package com.vlad.dao;

import com.vlad.model.University;
import org.hibernate.Criteria;

import java.util.List;

public interface UniversityDao {

    public University getUniversity(Long id);


    public Long saveUniversity(University st);


    public List<University> listAllUniversity();


    public void updateUniversity(University un);


    public void deleteUniversity(University un);
}
