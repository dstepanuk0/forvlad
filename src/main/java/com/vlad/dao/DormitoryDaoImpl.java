package com.vlad.dao;

import com.vlad.model.Dormitory;
import com.vlad.model.Student;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("dormitoryDao")
@Transactional
public class DormitoryDaoImpl extends AbstractDao<Long, Dormitory> implements DormitoryDao{
    @Override
    public Dormitory getDormitory(Long id) {
        return getByKey(id);
    }

    @Override
    public Long saveDormitory(Dormitory st) {
        persist(st);
        return st.getId();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List<Dormitory> listAllDormitory() {
        Criteria criteria = createEntityCriteria();
        return (List) criteria.list();
    }

    @Override
    public void updateDormitory(Dormitory st) {
        update(st);
    }

    @Override
    public void deleteDormitory(Dormitory st) {
        delete(st);
    }
}
