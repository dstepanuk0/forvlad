package com.vlad.dao;

import com.vlad.model.Dormitory;
import com.vlad.model.Scholarship;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("scholarshipDao")
@Transactional
public class ScholarshipDaoImpl extends AbstractDao<Long, Scholarship> implements ScholarshipDao{
    @Override
    public Scholarship getScholarship(Long id) {
        return getByKey(id);
    }

    @Override
    public Long saveScholarship(Scholarship st) {
        persist(st);
        return st.getId();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List<Scholarship> listAllScholarship() {
        Criteria criteria = createEntityCriteria();
        return (List) criteria.list();
    }

    @Override
    public void updateScholarship(Scholarship st) {
        update(st);
    }

    @Override
    public void deleteScholarship(Scholarship st) {
        delete(st);
    }
}
