package com.vlad.dao;

import com.vlad.model.Scholarship;
import com.vlad.model.Subject;

import java.util.List;

public interface ScholarshipDao {
    public Scholarship getScholarship(Long id);


    public Long saveScholarship(Scholarship sb);


    public List<Scholarship> listAllScholarship();


    public void updateScholarship(Scholarship sb);


    public void deleteScholarship(Scholarship sb);
}
