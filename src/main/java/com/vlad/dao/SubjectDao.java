package com.vlad.dao;

import com.vlad.model.Subject;
import com.vlad.model.University;

import java.util.List;

public interface SubjectDao {
    public Subject getSubject(Long id);


    public Long saveSubject(Subject sb);


    public List<Subject> listAllSubject();


    public void updateSubject(Subject sb);


    public void deleteSubject(Subject sb);
}
