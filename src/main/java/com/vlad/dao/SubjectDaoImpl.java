package com.vlad.dao;

import com.vlad.model.Dormitory;
import com.vlad.model.Student;
import com.vlad.model.Subject;
import com.vlad.model.University;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("subjectDao")
@Transactional
public class SubjectDaoImpl extends AbstractDao<Long, Subject> implements SubjectDao{

    @Override
    public Subject getSubject(Long id) {
        return getByKey(id);
    }

    @Override
    public Long saveSubject(Subject sb) {
        persist(sb);
        return sb.getId();
    }

    @Override
    public List<Subject> listAllSubject() {
        Criteria criteria = createEntityCriteria();
        return (List) criteria.list();
    }

    @Override
    public void updateSubject(Subject sb) {
        update(sb);
    }

    @Override
    public void deleteSubject(Subject sb) {
        delete(sb);
    }
}
