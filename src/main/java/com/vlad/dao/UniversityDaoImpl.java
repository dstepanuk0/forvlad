package com.vlad.dao;

import com.vlad.model.Dormitory;
import com.vlad.model.Student;
import com.vlad.model.University;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("universityDao")
@Transactional
public class UniversityDaoImpl extends AbstractDao<Long, University> implements UniversityDao{
    @Override
    public University getUniversity(Long id) {
        return getByKey(id);
    }

    @Override
    public Long saveUniversity(University st) {
        persist(st);
        return st.getId();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List<University> listAllUniversity() {
        Criteria criteria = createEntityCriteria();
        return (List) criteria.list();
    }

    @Override
    public void updateUniversity(University un) {
        update(un);
    }

    @Override
    public void deleteUniversity(University un) {
        delete(un);
    }
}
