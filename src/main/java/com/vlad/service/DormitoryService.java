package com.vlad.service;

import com.vlad.model.Dormitory;
import com.vlad.model.Student;

import java.util.List;

public interface DormitoryService {
    Dormitory getDormitory(Long id);

    Long saveDormitory(Dormitory st);

    List<Dormitory> listAllDormitory();

    void update(Long id, Dormitory st);

    void delete(Long id);

    boolean isDormitoryUnique(Long id);
}
