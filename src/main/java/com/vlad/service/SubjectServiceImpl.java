package com.vlad.service;

import com.vlad.dao.ScholarshipDao;
import com.vlad.dao.SubjectDao;
import com.vlad.model.Scholarship;
import com.vlad.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("subjectService")
public class SubjectServiceImpl implements SubjectService{
    @Autowired
    private SubjectDao subjectDao;

    @Override
    public Subject getSubject(Long id) {
        return subjectDao.getSubject(id);
    }

    @Override
    public Long saveSubject(Subject st) {
        return subjectDao.saveSubject(st);
    }

    @Override
    public List<Subject> listAllSubject() {
        return subjectDao.listAllSubject();
    }

    @Override
    public void update(Long id, Subject st) {
        Subject stEntity = subjectDao.getSubject(id);
        if (stEntity != null) {
            stEntity.setName(st.getName());
            subjectDao.updateSubject(stEntity);
        }
    }

    @Override
    public void delete(Long id) {
        Subject stEntity = subjectDao.getSubject(id);
        if (stEntity != null) {
            subjectDao.deleteSubject(stEntity);
        }
    }

    @Override
    public boolean isSubjectUnique(Long id) {
        Subject subject = subjectDao.getSubject(id);
        return (subject == null || (id != null & !id.equals(subject.getId())));
    }
}
