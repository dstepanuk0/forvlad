package com.vlad.service;

import com.vlad.dao.DormitoryDao;
import com.vlad.dao.ScholarshipDao;
import com.vlad.model.Dormitory;
import com.vlad.model.Scholarship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("scholarshipService")
public class ScholarshipServiceImpl implements ScholarshipService{
    @Autowired
    private ScholarshipDao scholarshipDao;

    @Override
    public Scholarship getScholarship(Long id) {
        return scholarshipDao.getScholarship(id);
    }

    @Override
    public Long saveScholarship(Scholarship st) {
        return scholarshipDao.saveScholarship(st);
    }

    @Override
    public List<Scholarship> listAllScholarship() {
        return scholarshipDao.listAllScholarship();
    }

    @Override
    public void update(Long id, Scholarship st) {
        Scholarship stEntity = scholarshipDao.getScholarship(id);
        if (stEntity != null) {
            stEntity.setName(st.getName());
            stEntity.setSize(st.getSize());
            scholarshipDao.updateScholarship(stEntity);
        }
    }

    @Override
    public void delete(Long id) {
        Scholarship stEntity = scholarshipDao.getScholarship(id);
        if (stEntity != null) {
            scholarshipDao.deleteScholarship(stEntity);
        }
    }

    @Override
    public boolean isScholarshipUnique(Long id) {
        Scholarship scholarship = scholarshipDao.getScholarship(id);
        return (scholarship == null || (id != null & !id.equals(scholarship.getId())));
    }
}
