package com.vlad.service;

import java.util.List;

import com.vlad.DTO.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vlad.dao.StudentDao;
import com.vlad.model.Student;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDao studentDao;
	@Autowired
	private StudentDTO studentDTO;

	@Override
	public StudentDTO getStudent(Long id) {

		return studentDTO.toDto(studentDao.getStudent(id));
	}

	@Override
	public Long saveStudent(Student st) {
		return studentDao.saveStudent(st);
	}

	@Override
	public List<Student> listAllStudents() {
		return studentDao.listAllStudents();
	}

	@Override
	public void update(Long id, Student st) {
		Student stEntity = studentDao.getStudent(id);
		if (stEntity != null) {
			stEntity.setFirstName(st.getFirstName());
			stEntity.setLastName(st.getLastName());
			stEntity.setCourse(st.getCourse());
			studentDao.updateStudent(stEntity);
		}
	}

	@Override
	public void delete(Long id) {
		Student stEntity = studentDao.getStudent(id);
		if (stEntity != null) {
			studentDao.deleteStudent(stEntity);
		}
	}

	@Override
	public boolean isStudentUnique(Long id) {
		Student student = studentDao.getStudent(id);
		return (student == null || (id != null & !id.equals(student.getId())));
	}

}
