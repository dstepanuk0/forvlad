package com.vlad.service;

import com.vlad.dao.UniversityDao;
import com.vlad.model.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("universityService")
public class UniversityServiceImpl implements UniversityService{
    @Autowired
    private UniversityDao universityDao;

    @Override
    public University getUniversity(Long id) {
        return universityDao.getUniversity(id);
    }

    @Override
    public Long saveUniversity(University st) {
        return universityDao.saveUniversity(st);
    }

    @Override
    public List<University> listAllUniversity() {
        return universityDao.listAllUniversity();
    }

    @Override
    public void update(Long id, University st) {
        University stEntity = universityDao.getUniversity(id);
        if (stEntity != null) {
            stEntity.setName(st.getName());
            universityDao.updateUniversity(stEntity);
        }
    }

    @Override
    public void delete(Long id) {
        University stEntity = universityDao.getUniversity(id);
        if (stEntity != null) {
            universityDao.deleteUniversity(stEntity);
        }
    }

    @Override
    public boolean isUniversityUnique(Long id) {
        University university = universityDao.getUniversity(id);
        return (university == null || (id != null & !id.equals(university.getId())));
    }
}
