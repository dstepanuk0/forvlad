package com.vlad.service;

import java.util.List;

import com.vlad.DTO.StudentDTO;
import com.vlad.model.Student;

/**
 * Student Service Interface for Simple Spring MVC CRUD App
 * 
 * @author Mandar Pandit
 */
public interface StudentService {

	StudentDTO getStudent(Long id);

	Long saveStudent(Student st);

	List<Student> listAllStudents();

	void update(Long id, Student st);

	void delete(Long id);

	boolean isStudentUnique(Long id);
}
