package com.vlad.service;

import com.vlad.dao.DormitoryDao;
import com.vlad.dao.StudentDao;
import com.vlad.model.Dormitory;
import com.vlad.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("dormitoryService")
public class DormitoryServiceImpl implements DormitoryService{
    @Autowired
    private DormitoryDao dormitoryDao;

    @Override
    public Dormitory getDormitory(Long id) {
        return dormitoryDao.getDormitory(id);
    }

    @Override
    public Long saveDormitory(Dormitory st) {
        return dormitoryDao.saveDormitory(st);
    }

    @Override
    public List<Dormitory> listAllDormitory() {
        return dormitoryDao.listAllDormitory();
    }

    @Override
    public void update(Long id, Dormitory st) {
        Dormitory stEntity = dormitoryDao.getDormitory(id);
        if (stEntity != null) {
            stEntity.setName(st.getName());
            stEntity.setNumber(st.getNumber());
            dormitoryDao.updateDormitory(stEntity);
        }
    }

    @Override
    public void delete(Long id) {
        Dormitory stEntity = dormitoryDao.getDormitory(id);
        if (stEntity != null) {
            dormitoryDao.deleteDormitory(stEntity);
        }
    }

    @Override
    public boolean isDormitoryUnique(Long id) {
        Dormitory dormitory = dormitoryDao.getDormitory(id);
        return (dormitory == null || (id != null & !id.equals(dormitory.getId())));
    }
}
