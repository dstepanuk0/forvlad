package com.vlad.service;

import com.vlad.model.Dormitory;
import com.vlad.model.Scholarship;

import java.util.List;

public interface ScholarshipService {
    Scholarship getScholarship(Long id);

    Long saveScholarship(Scholarship st);

    List<Scholarship> listAllScholarship();

    void update(Long id, Scholarship st);

    void delete(Long id);

    boolean isScholarshipUnique(Long id);
}
