package com.vlad.service;

import com.vlad.model.Scholarship;
import com.vlad.model.Subject;

import java.util.List;

public interface SubjectService {
    Subject getSubject(Long id);

    Long saveSubject(Subject st);

    List<Subject> listAllSubject();

    void update(Long id, Subject st);

    void delete(Long id);

    boolean isSubjectUnique(Long id);
}
