package com.vlad.service;

import com.vlad.model.University;

import java.util.List;

public interface UniversityService {
    University getUniversity(Long id);

    Long saveUniversity(University st);

    List<University> listAllUniversity();

    void update(Long id, University st);

    void delete(Long id);

    boolean isUniversityUnique(Long id);
}
