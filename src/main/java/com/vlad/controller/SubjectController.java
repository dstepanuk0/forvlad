package com.vlad.controller;

import com.vlad.model.Student;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.vlad.model.Subject;
import com.vlad.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    SubjectService subjectService;


    @RequestMapping(value =  "/list" , method = RequestMethod.GET)
    public ModelAndView welcome(ModelMap model) {
        ModelAndView mv = new ModelAndView();
        List<Subject> list = subjectService.listAllSubject();
        model.addAttribute("allsubjects", list);
        mv.setViewName("allSubjects");
        return mv;
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.GET)
    public String newStudentForm(ModelMap model) {
        Student newStudent = new Student();
        model.addAttribute("subject", newStudent);
        model.addAttribute("save", false);
        return "addSubjectForm";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public ModelAndView saveSubject(@Valid Subject subject, BindingResult result, ModelMap model) {
        ModelAndView mv = new ModelAndView();
        String name  = subject.getName();
        if (name == null) {
            model.addAttribute("subject", subject);
            model.addAttribute("error", "Name can't be empty.");
            mv.setViewName("addSubjectForm");
        } else {
            subjectService.saveSubject(subject);
            List<Subject> list = subjectService.listAllSubject();
            model.addAttribute("allsubjects", list);
            model.addAttribute("success", "Subject " + subject.getName() + " added successfully.");
            mv.setViewName("allSubjects");
        }
        return mv;
    }

    @RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
    public ModelAndView deleteSubject(@PathVariable Long id, ModelMap model) {
        ModelAndView mv = new ModelAndView();
        Subject subject = subjectService.getSubject(id);
        subjectService.delete(id);
        List<Subject> list = subjectService.listAllSubject();
        model.addAttribute("allsubjects", list);
        model.addAttribute("success", "Subject " + subject.getName() + " deleted successfully.");
        mv.setViewName("allSubjects");
        return mv;
    }
}
